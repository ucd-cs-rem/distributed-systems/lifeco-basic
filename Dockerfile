FROM eclipse-temurin:21-jdk-alpine
COPY target/lifeco-1.0.jar /lifeco-1.0.jar
CMD java -cp /lifeco-1.0.jar client.Main
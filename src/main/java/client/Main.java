package client;

import java.text.NumberFormat;

import service.auldfellas.AFQService;
import service.broker.LocalBrokerService;
import service.core.BrokerService;
import service.core.ClientInfo;
import service.core.Quotation;
import service.dodgygeezers.DGQService;
import service.core.Constants;
import service.girlsallowed.GAQService;
import service.registry.ServiceRegistry;

public class Main {
	static {
		// Create the services and bind them to the registry.
		ServiceRegistry.bind(Constants.GIRLS_ALLOWED_SERVICE, new GAQService());
		ServiceRegistry.bind(Constants.AULD_FELLAS_SERVICE, new AFQService());
		ServiceRegistry.bind(Constants.DODGY_GEEZERS_SERVICE, new DGQService());
		ServiceRegistry.bind(Constants.BROKER_SERVICE, new LocalBrokerService());
	}
	
	/**
	 * This is the starting point for the application. Here, we must
	 * get a reference to the Broker Service and then invoke the
	 * getQuotations() method on that service.
	 * 
	 * Finally, you should print out all quotations returned
	 * by the service.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		BrokerService brokerService = ServiceRegistry.lookup(Constants.BROKER_SERVICE, BrokerService.class);
 
		int numc = 0;
		for (ClientInfo info : clients) {
		    numc++;
		    System.out.println("\n\n|=================================================CLIENT INFO 0" + numc + "==================================================|");
		    displayProfile(info);

		    // Retrieve quotations from the broker and display them...
		    int numq = 0;
		    for(Quotation quotation : brokerService.getQuotations(info)) {
		        numq++;
		        System.out.println("\n" + "|==================================================QUOTATION 0" + numq + "===================================================|");
		        displayQuotation(quotation);
		    }

		    // Print a couple of lines between each client
		    System.out.println("                                   ********************END********************                                   " + "\n");
		}
	    }

	    /**
	     * Display the client info nicely.
	     *
	     * @param info Client info
	     */
	    public static void displayProfile(ClientInfo info) {
		System.out.println("|=================================================================================================================|");
		System.out.println("|                                     |                                     |                                     |");
		System.out.println(
		        "| Name: " + String.format("%1$-29s", info.name) +
		                " | Gender: " + String.format("%1$-27s", (info.gender==ClientInfo.MALE?"Male":"Female")) +
		                " | Age: " + String.format("%1$-30s", info.age)+" |");
		System.out.println(
		        "| Weight/Height: " + String.format("%1$-20s", info.weight+"kg/"+info.height+"m") +
		                " | Smoker: " + String.format("%1$-27s", info.smoker?"YES":"NO") +
		                " | Medical Problems: " + String.format("%1$-17s", info.medicalIssues?"YES":"NO")+" |");
		System.out.println("|                                     |                                     |                                     |");
		System.out.println("|=================================================================================================================|");
	    }

	    /**
	     * Display a quotation nicely - note that the assumption is that the quotation will follow
	     * immediately after the profile (so the top of the quotation box is missing).
	     *
	     * @param quotation From quotation services
	     */
	    public static void displayQuotation(Quotation quotation) {
		System.out.println(
		        "| Company: " + String.format("%1$-26s", quotation.company) +
		                " | Reference: " + String.format("%1$-24s", quotation.reference) +
		                " | Price: " + String.format("%1$-28s", NumberFormat.getCurrencyInstance().format(quotation.price))+" |");
		System.out.println("|=================================================================================================================|");
	    }

	    /**
	     * Test Data
	     */
	    public static final ClientInfo[] clients = {
		    new ClientInfo("One Two", ClientInfo.FEMALE, 49, 1.5494, 80, false, false),
		    new ClientInfo("Three Four", ClientInfo.MALE, 65, 1.6, 100, true, true),
		    new ClientInfo("Five Six'", ClientInfo.FEMALE, 21, 1.78, 65, false, false),
		    new ClientInfo("Seven Eight", ClientInfo.MALE, 49, 1.8, 120, false, true),
		    new ClientInfo("Nine Ten", ClientInfo.MALE, 55, 1.9, 75, true, false),
		    new ClientInfo("Eleven Twelve", ClientInfo.MALE, 35, 0.45, 1.6, false, false)
	    };
}
